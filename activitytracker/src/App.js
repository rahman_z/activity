import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import NavBar from "./components/NavBar";
import ActivityList from "./components/ActivityList";
import EditActivity from "./components/EditActivity";
import CreateActivity from "./components/CreateActivity";
import createUser from "./components/CreateUser";

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar />
        <Route path="/" exact={true} component={ActivityList} />
        <Route path="/edit/:id" exact={true} component={EditActivity} />
        <Route path="/activity" exact={true} component={CreateActivity} />
        <Route path="/user" exact={true} component={createUser} />
      </Router>
    </div>
  );
}

export default App;
