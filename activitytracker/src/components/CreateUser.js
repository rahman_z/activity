import React, { useState } from "react";
import axios from "axios";

function CreateUser(props) {
  const [username, setUsername] = useState("");

  function submitHandler(username) {
    axios
      .post("http://localhost:5000/users/add", { username })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
    setUsername("");
  }

  return (
    <div>
      <p> Add new User</p>
      <form
        style={{
          display: "flex",
          flexDirection: "row",
          alignContent: "center",
          justifyContent: "center",
        }}
      >
        <input
          type="text"
          placeholder="username"
          onChange={(e) => setUsername(e.target.value)}
          value={username}
        />
        <button type="submit" onClick={() => submitHandler(username)}>
          submit
        </button>
      </form>
    </div>
  );
}

export default CreateUser;
