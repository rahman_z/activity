import React from "react";

import { Link } from "react-router-dom";

export default function NavBar() {
  return (
    <div>
      <nav style={{ backgroundColor: "black", minHeight: "70px" }}>
        <Link to="/" style={{ color: "white", textDecoration: "none" }}>
          Activity Tracker
        </Link>
        <div>
          <ul
            style={{
              display: "flex",
              flexDirection: "row",
              alignContent: "space-evenly",
              justifyContent: "space-evenly",
            }}
          >
            <li style={{ listStyleType: "none" }}>
              <Link to="/" style={{ color: "white", textDecoration: "none" }}>
                Activities Available
              </Link>{" "}
            </li>
            <li style={{ listStyleType: "none" }}>
              <Link
                to="/activity"
                style={{ color: "white", textDecoration: "none" }}
              >
                Create Activity
              </Link>{" "}
            </li>
            <li style={{ listStyleType: "none" }}>
              <Link
                to="/user"
                style={{ color: "white", textDecoration: "none" }}
              >
                Create User
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}
