import React, { useState, useEffect } from "react";
import axios from "axios";
import Activity from "./Activity";

function ActivityList(props) {
  const [activityList, setActivityList] = useState([]);

  useEffect(() => {
    axios
      .get("http://localhost:5000/activity")
      .then((response) => {
        setActivityList(response.data);
      })
      .catch((err) => console.log(err));
  }, []);

  function deleteActivity(id) {
    axios
      .delete(`http://localhost:5000/activity/delete/${id}`)
      .then((res) => console.log(res.data));

    const afterDeletion = activityList.filter((el) => el._id !== id);

    setActivityList(afterDeletion);
  }

  function displayList() {
    return activityList.map((current) => {
      return (
        <Activity
          deleteActivity={deleteActivity}
          activity={current}
          key={current._id}
        />
      );
    });
  }

  return (
    <div>
      <table cellSpacing="15" cellPadding="20" border="1">
        <thead>
          <tr>
            <th>username</th>
            <th>Name</th>
            <th>Duration</th>
            <th>Date</th>
            <th>options</th>
          </tr>
        </thead>
        <tbody>{displayList()}</tbody>
      </table>
    </div>
  );
}

export default ActivityList;
