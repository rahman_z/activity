import React from "react";
import { Link } from "react-router-dom";

function Activity(props) {
  return (
    <tr key={props.activity._id}>
      <td>{props.activity.username}</td>
      <td>{props.activity.Name}</td>
      <td>{props.activity.Duration}</td>
      <td>{props.activity.Date}</td>
      <td>
        <Link to={`/edit/${props.activity._id}`}>EDIT</Link> |{" "}
        <Link to="#" onClick={() => props.deleteActivity(props.activity._id)}>
          DELETE
        </Link>
      </td>
    </tr>
  );
}

export default Activity;
