import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

function EditActivity(props) {
  const [username, setUsername] = useState("");
  const [Name, setname] = useState("");
  const [Duration, setDuration] = useState("");
  const [Date, setdate] = useState("");
  const [users, setUsers] = useState([]);

  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`http://localhost:5000/activity/${id}`)
      .then((res) => {
        setUsername(res.data.username);
        setname(res.data.Name);
        setDuration(res.data.Duration);
        setdate(res.data.Date);
      })
      .catch((err) => console.log(err));

    axios.get("http://localhost:5000/users").then((response) => {
      if (response.data.length > 0) {
        setUsers(response.data.map((user) => user.username));
      }
    });
  }, [id]);

  function handleSubmit(username, Name, Duration, Date) {
    const activity = {
      username: username,
      Name: Name,
      Duration: Duration,
      Date: Date,
    };

    axios
      .put(`http://localhost:5000/activity/update/${id}`, activity)
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  }

  return (
    <div>
      <form
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          alignContent: "center",
        }}
      >
        <label>username: </label>
        <select onChange={(e) => setUsername(e.target.value)} value={username}>
          {users.map((user) => {
            return (
              <option key={user} value={user}>
                {user}
              </option>
            );
          })}
        </select>
        &nbsp;
        <label>Name :</label>
        <input
          type="text"
          onChange={(e) => setname(e.target.value)}
          value={Name}
        />
        &nbsp;
        <label>Duration :</label>
        <input
          type="number"
          onChange={(e) => setDuration(e.target.value)}
          value={Duration}
        />
        &nbsp;
        <label>Date :</label>
        <input
          type="date"
          onChange={(e) => setdate(e.target.value)}
          value={Date}
        />
        <button onClick={() => handleSubmit(username, Name, Duration, Date)}>
          submit
        </button>
      </form>
    </div>
  );
}

export default EditActivity;
