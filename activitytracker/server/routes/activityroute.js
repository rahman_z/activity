const router = require("express").Router();
let Activity = require("../models/activitymodel");

router.route("/").get((req, res) => {
  Activity.find()
    .then((acts) => res.status(200).json(acts))
    .catch((err) => res.status(400).json("message :" + err));
});

router.route("/add").post((req, res) => {
  const username = req.body.username;
  const Name = req.body.Name;
  const Duration = req.body.Duration;
  const Date = req.body.Date;

  const newActivity = new Activity({
    username,
    Name,
    Duration,
    Date,
  });

  newActivity
    .save()
    .then(() => res.status(200).json("activity addeed"))
    .catch((err) => res.status(400).json("message :" + err));
});

router.route("/:id").get((req, res) => {
  Activity.findById(req.params.id)
    .then((act) => res.status(200).json(act))
    .catch((err) => res.status(400).json("message :" + err));
});

router.route("/update/:id").put((req, res) => {
  Activity.findById(req.params.id)
    .then((act) => {
      act.username = req.body.username;
      act.Name = req.body.Name;
      act.Duration = req.body.Duration;
      act.Date = req.body.Date;

      act
        .save()
        .then(() => res.status(200).json("updated"))
        .catch((err) => res.status(400).json("message : " + err));
    })
    .catch((err) => res.status(400).json("message : " + err));
});

router.route("/delete/:id").delete((req, res) => {
  Activity.findByIdAndDelete(req.params.id)
    .then(() => res.status(200).json("deleted"))
    .catch((err) => res.status(400).json("message : " + err));
});

module.exports = router;
