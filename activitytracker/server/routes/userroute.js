const router = require("express").Router();
let User = require("../models/usermodel");

router.route("/").get((req, res) => {
  User.find()
    .then((users) => res.json(users))
    .catch((err) => res.status(400).json("message :" + err));
});

router.route("/add").post((req, res) => {
  const username = req.body.username;

  const newUser = new User({ username });

  newUser
    .save()
    .then(() => res.status(200).json("user added"))
    .catch((err) => res.status(400).json("message : " + err));
});

router.route("/:id").get((req, res) => {
  User.findById(req.params.id)
    .then((user) => res.status(200).json(user))
    .catch((err) => res.status(400).json("message : " + err));
});

router.route("/update/:id").put((req, res) => {
  User.findById(req.params.id).then((act) => {
    act.username = req.body.username;

    act
      .save()
      .then(() => res.status(200).json("updated"))
      .catch((err) => res.status(400).json("message : " + err));
  });
});

router.route("/delete/:id").delete((req, res) => {
  User.findByIdAndDelete(req.params.id)
    .then((user) => res.status(200).json("deleted"))
    .catch((err) => res.status(400).json("message : " + err));
});

module.exports = router;
