const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const activitySchema = new Schema({
  username: {
    type: String,
    required: true,
    minlength: 3,
  },

  Name: {
    type: String,
    required: true,
    minlength: 3,
  },
  Duration: {
    type: Number,
    required: true,
  },
  Date: {
    type: Date,
    required: true,
  },
});

const Activity = mongoose.model("Activity", activitySchema);

module.exports = Activity;
