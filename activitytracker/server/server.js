const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
require("dotenv").config();
const app = express();

const PORT = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

const URL = process.env.DB_CONNECTION_URL;
mongoose.connect(URL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});

const connection = mongoose.connection;
connection.once("open", () => {
  console.log("DB connection success");
});

const activityrouter = require("./routes/activityroute");
const userrouter = require("./routes/userroute");

app.use("/activity", activityrouter);
app.use("/users", userrouter);

app.listen(PORT, () => {
  console.log("server is running");
});
